# CONFAB MH #



### What is CONFAB MH? ###

**CONFAB MH** (**C**onversations **O**n **N**etworks **F**acilitated **A**s **B**usiness – **M**ental **H**ealth) is an app within **MS Teams** that helps individuals with mental health counseling and **AAC** (**A**ugmentative and **A**lternative **C**ommunication) healthcare resources.